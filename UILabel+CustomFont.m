//
//  UILabel+CustomFont.m
//  ausopen-iphone-2014
//
//  Created by Christian Linares on 10/25/13.
//  Copyright (c) 2013 ibm. All rights reserved.
//

#import "UILabel+CustomFont.h"

@implementation UILabel (CustomFont)

- (void) addCustomFont {
    [self setFont:[UIFont fontWithName:FONT_1 size:self.font.pointSize]];
}

- (void) addCustomFontWithSize : (CGFloat) size {
    [self setFont:[UIFont fontWithName:FONT_1 size:size]];
}

- (void) addCustomFontWithSize : (CGFloat) size andHexColorCode : (NSString*) colorHexStr {
    [self setFont:[UIFont fontWithName:FONT_1 size:size]];
    [self setTextColor:[Utils colorWithHexString:colorHexStr]];
}



@end
