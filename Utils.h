//
//  Utils.h
//  WimbledoniPad
//
//  Created by Christian Linares on 5/8/13.
//  Copyright (c) 2013 ibmi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventData.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MFMailComposeViewController.h>

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define isWideScreen  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE

#define D_IPHONE_1G @"iPhone 1G"
#define D_IPHONE_3G @"iPhone 3G"
#define D_IPHONE_3GS @"iPhone 3GS"
#define D_IPHONE_4 @"iPhone 4"
#define D_IPHONE_VZN4 @"Verizon iPhone 4"
#define D_IPHONE_4S @"iPhone 4S"
#define D_IPHONE_5GSM @"iPhone 5 (GSM)"
#define D_IPHONE_5GSM_CDMA @"iPhone 5 (GSM+CDMA)"
#define D_IPODT_1G @"iPod Touch 1G"
#define D_IPODT_2G @"iPod Touch 2G"
#define D_IPODT_3G @"iPod Touch 3G"
#define D_IPODT_4G @"iPod Touch 4G"
#define D_IPODT_5G @"iPod Touch 5G"
#define D_IPAD_1 @"iPad"
#define D_IPAD_2_WIFI @"iPad 2 (WiFi)"
#define D_IPAD_2_GSM @"iPad 2 (GSM)"
#define D_IPAD_2_CDMA @"iPad 2 (CDMA)"
#define D_IPADMINI_WIFI @"iPad Mini (WiFi)"
#define D_IPADMINI_GSM @"iPad Mini (GSM)"
#define D_IPADMINI_GSM_CDMA @"iPad Mini (GSM+CDMA)"
#define D_IPAD_3_WIFI @"iPad 3 (WiFi)"
#define D_IPAD_3_GSM_CDMA @"iPad 3 (GSM+CDMA)"
#define D_IPAD_3_GSM @"iPad 3 (GSM)"
#define D_IPAD_4_WIFI @"iPad 4 (WiFi)"
#define D_IPAD_4_GSM @"iPad 4 (GSM)"
#define D_IPAD_4_GSM_CDMA @"iPad 4 (GSM+CDMA)"
#define D_SIM @"Simulator"

@interface Utils : NSObject <MFMailComposeViewControllerDelegate>

@property(nonatomic,retain) NSString *hash;
@property(nonatomic,retain) NSMutableSet *pinUpdateQueue;
@property(nonatomic,retain) NSMutableSet *arrayFilters;
@property(nonatomic,weak) UIViewController *mailVC;

+(Utils*)shared;

+ (NSArray*) getSortedArray : (NSArray*) array;
+ (BOOL) is6_0orAbove;
+ (UIView*) createNavTitleView : (NSString*) text;
+ (UIColor*)colorWithHexString:(NSString*)hex;
+ (void) report_memory;
+ (NSString *) platformString;
+ (NSMutableArray*) readPlist : (NSString*) fileName;
+ (NSMutableArray*) readPlistWithFileName :  (NSString*) fileName withArrayName : (NSString*) arrayName;

- (void) sendEmail : (UIViewController*) vc withSubject : (NSString*) subject withMessageBody : (NSString*) msgBody withRecipient : (NSString*) recipient;


@end
