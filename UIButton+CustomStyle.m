//
//  UILabel+CustomFont.m
//  ausopen-iphone-2014
//
//  Created by Christian Linares on 10/25/13.
//  Copyright (c) 2013 ibm. All rights reserved.
//

#import "UIButton+CustomStyle.h"

@implementation UIButton (CustomStyle)

- (void) addAOStyle {
    [self setBackgroundColor:[Utils colorWithHexString:@"00adfc"]];
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.cornerRadius = 7.0f;
}


@end
