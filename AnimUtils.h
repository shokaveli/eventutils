//
//  AnimUtils.h
//  Christian's Holy Grail of Animation methods for your programming pleasure
//
//  Created by Christian Linares on 5/26/13.
//  Copyright (c) 2013 Shokware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface AnimUtils : NSObject

+(AnimUtils*)shared;

+ (void) attachPopUpAnimation : (CALayer*) layer;
+ (void) attachPopUpAnimationPins : (CALayer*) layer : (float) zoomScale;
+ (void) attachPopUpAnimationPopUps : (CALayer*) layer;
+ (CAKeyframeAnimation*) attachPopUpHideAnimation;
+ (void) attachZoomInAnimation : (CALayer*) layer;
+ (void) attachZoomOutAnim2 : (CALayer*) layer;
+ (CAKeyframeAnimation*) attachZoomOutAnim : (CALayer*) layer;
+ (void) attach360RotationAnim : (CALayer*) layer;
+ (CAKeyframeAnimation*) getZoomInAnimation : (CALayer*) layer;
+ (CAKeyframeAnimation*)dockBounceAnimationWithViewHeight:(CGFloat)viewHeight;
+ (void) attach180RotationAnim_Close : (CALayer*) layer;
+ (void) attach180RotationAnim_Open : (CALayer*) layer;
+ (void) transitionImageWithFlip : (UIImageView*) imageView newImage : (UIImage*) newImage;
+ (void) fadeInAnimation : (UIView *) view;

@end
