//
//  UIPlaceHolderTextView.h
//  ausopen-iphone-2014
//
//  Created by Christian Linares on 10/25/13.
//  Copyright (c) 2013 ibm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPlaceHolderTextView : UITextView

// Named .placeholderText, in case UITextView gains
// a .placeholder property (like UITextField) in future iOS versions.
@property (nonatomic, copy) NSString *placeholderText;

@end
