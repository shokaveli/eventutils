//
//  NSDate+TwitterAgeString.h
//  usta-ipad-2012
//
//  Created by wismar on 7/30/12.
//  Copyright (c) 2012 IBM. All rights reserved.
//

#import <Foundation/Foundation.h>

#define WWS_SECONDS_PER_MINUTE 60
#define WWS_SECONDS_PER_HOUR 3600
#define WWS_SECONDS_PER_DAY (3600*24)



@interface NSDate (TwitterAgeString)

- (NSString*) twitterShortAgeString;

@end
