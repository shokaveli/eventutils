//
//  NSDate+TwitterAgeString.m
//  usta-ipad-2012
//
//  Created by wismar on 7/30/12.
//  Copyright (c) 2012 IBM. All rights reserved.
//

#import "NSDate+TwitterAgeString.h"

@implementation NSDate (TwitterAgeString)


- (NSString*) twitterShortAgeString {
    
    NSDate* rightNow = [NSDate date];
    NSTimeInterval tweetInterval = [rightNow timeIntervalSinceDate:self];
    NSString* ageString = nil;
    
    if (tweetInterval < WWS_SECONDS_PER_MINUTE) {
        
        ageString = [NSString stringWithFormat:@"%.0fs", tweetInterval];
        
        
    } else if (tweetInterval < WWS_SECONDS_PER_HOUR) {
        
        ageString = [NSString stringWithFormat:@"%.0fm",tweetInterval/WWS_SECONDS_PER_MINUTE];
        
    } else if (tweetInterval < WWS_SECONDS_PER_DAY) {
        
        ageString = [NSString stringWithFormat:@"%.0fh",tweetInterval/WWS_SECONDS_PER_HOUR];
        
    } else {
        
        ageString = [NSString stringWithFormat:@"%.0fd",tweetInterval/WWS_SECONDS_PER_DAY];
        
    }
    
    return ageString;
}





@end
