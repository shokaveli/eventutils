//
//  ToggleButton.m
//  ausopen-ipad-2014
//
//  Created by Christian Linares on 11/13/13.
//  Copyright (c) 2013 ibm. All rights reserved.
//

#import "ToggleButton.h"
#import "AnimUtils.h"
#import <EventData.h>

@implementation ToggleButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void) layoutSubviews {
    [super layoutSubviews];

    self.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapRecog = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
    [self addGestureRecognizer:tapRecog];

    //Default State
    if ([[[EventData sharedData] favoritePlayersController] isFavoritePlayer:[_player playerId]]) {
        [self setImage:[UIImage imageNamed:@"yellowStarChecked"]];
    }else{
        [self setImage:[UIImage imageNamed:@"yellowStarUnchecked"]];
    }
    
}

- (void) tapHandler : (UIGestureRecognizer*) tapRec {
    
    if ([[[EventData sharedData] favoritePlayersController] isFavoritePlayer:[_player playerId]]) {
        
        [[[EventData sharedData] favoritePlayersController] removeFavoritePlayer:[_player playerId]];
        [self setImage:[UIImage imageNamed:@"yellowStarUnchecked"]];
        
        if (_shouldDissapearOnTap) {
            [AnimUtils attachZoomOutAnim2:tapRec.view.superview.layer];
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableView" object:self userInfo:nil];
            });
        }
        
        [Track track:[NSString stringWithFormat:@"Alerts:Favorite Remove:%@, %@",_player.lastName,_player.firstName]];
        
    }else{
        
        //Check if there are already 5 favorites
        if ([[[EventData sharedData] favoritePlayersController] favoritePlayersCount] == 200) {
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Favourite Players"
                                                                message:@"You may only favourite 10 players at a time, and you have already selected the maximum." delegate:self
                                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            
        }else{ //Add if not at max
            
            [[[EventData sharedData] favoritePlayersController] addFavoritePlayer:[_player playerId]];
            [self setImage:[UIImage imageNamed:@"yellowStarChecked"]];
            [Track track:[NSString stringWithFormat:@"Alerts:Favorite Add:%@, %@",_player.lastName,_player.firstName]];
        }
    }
    
    [AnimUtils attachPopUpAnimation:self.layer];
    
}

@end
