//
//  ToggleButton.h
//  ausopen-ipad-2014
//
//  Created by Christian Linares on 11/13/13.
//  Copyright (c) 2013 ibm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToggleButton : UIImageView

@property (strong,nonatomic) UIImage *offImage;
@property (strong,nonatomic) UIImage *onImage;
@property (strong, nonatomic) Players *player;
@property (assign, nonatomic) BOOL shouldDissapearOnTap;

@end
