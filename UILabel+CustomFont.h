//
//  UILabel+CustomFont.h
//  ausopen-iphone-2014
//
//  Created by Christian Linares on 10/25/13.
//  Copyright (c) 2013 ibm. All rights reserved.
//

#import <UIKit/UIKit.h>

#define FONT_1 @"Yanone Kaffeesatz"
#define COLOR_AOYELLOW @"f9ac18"

@interface UILabel (CustomFont)

- (void) addCustomFont;
- (void) addCustomFontWithSize : (CGFloat) size;
- (void) addCustomFontWithSize : (CGFloat) size andHexColorCode : (NSString*) colorHexStr;

@end
