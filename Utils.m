//
//  Utils.m
//  WimbledoniPad
//
//  Created by Christian Linares on 5/8/13.
//  Copyright (c) 2013 ibmi. All rights reserved.
//

#import "Utils.h"
#import <QuartzCore/QuartzCore.h>
#import "mach/mach.h"
#include <sys/types.h>
#include <sys/sysctl.h>

#define RADIANS_TO_DEGREES(__ANGLE__) ((__ANGLE__) / (float)M_PI * 180.0f)
#define DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) / 180.0 * M_PI)

@implementation Utils

static Utils* _shared = nil;

+(Utils*)shared
{
	@synchronized([Utils class])
	{
		if (!_shared){
			[[self alloc] init];
        }
        
		return _shared;
	}
    
	return nil;
}

+(id)alloc
{
	@synchronized([Utils class])
	{
		NSAssert(_shared == nil, @"Attempted to allocate a second instance of a singleton.");
		_shared = [super alloc];
        
		return _shared;
	}
    
	return nil;
}

+ (BOOL) is6_0orAbove {
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
        return YES;
    }else{
        
        return NO;
    }
    
}




+ (NSArray*) getSortedArray : (NSArray*) array {
    
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [array sortedArrayUsingDescriptors:sortDescriptors];
    
    return sortedArray;
    
}



+ (UIView*) createNavTitleView : (NSString*) text {
    
    UIView *view_Title = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 700, 30)];
    UILabel *lbl_Title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 700, 30)];
    [lbl_Title setTextAlignment:NSTextAlignmentCenter];
    [lbl_Title setFont:[UIFont boldSystemFontOfSize:18]];
    [lbl_Title setBackgroundColor:[UIColor clearColor]];
    [lbl_Title setTextColor:[UIColor whiteColor]];
    [lbl_Title setAdjustsFontSizeToFitWidth:YES];
    lbl_Title.text = text;
    view_Title.bounds = CGRectOffset(view_Title.bounds, 0, -5);
    [view_Title addSubview:lbl_Title];
    
    return view_Title;
}


+ (UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

+(void) report_memory {
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
                                   TASK_BASIC_INFO,
                                   (task_info_t)&info,
                                   &size);
    if( kerr == KERN_SUCCESS ) {
        NSLog(@"Memory in use (in bytes): %u", info.resident_size);
    } else {
        NSLog(@"Error with task_info(): %s", mach_error_string(kerr));
    }
}



+ (NSString *) platformString {
    
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    free(machine);
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
    return platform;
}

+ (NSMutableArray*) readPlist : (NSString*) fileName {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    
    if (filePath)
    {
        
        WWLog(@"File Exists Loading Filters\n");
        
        //Read plist into Dictionary
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        
        //Read array inside dict
        NSMutableArray *array_Filters = [[NSMutableArray alloc] initWithArray:[dict objectForKey:@"Items"]];
        
        return array_Filters;
        
    }else {
        
        NSLog(@"Error File Doesnt Exist");
        
        return [NSMutableArray new];
    }
}

+ (NSMutableArray*) readPlistWithFileName :  (NSString*) fileName withArrayName : (NSString*) arrayName {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    
    if (filePath)
    {
        
        WWLog(@"File Exists Loading Filters\n");
        
        //Read plist into Dictionary
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        
        //Read array inside dict
        NSMutableArray *array_Filters = [[NSMutableArray alloc] initWithArray:[dict objectForKey:arrayName]];
        
        return array_Filters;
        
    }else {
        
        NSLog(@"Error File Doesnt Exist");
        
        return [NSMutableArray new];
    }
}


- (void) sendEmail : (UIViewController*) vc withSubject : (NSString*) subject withMessageBody : (NSString*) msgBody withRecipient : (NSString*) recipient {
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setToRecipients:[[NSArray alloc] initWithObjects:recipient, nil]];
    [controller setSubject:subject];
    [controller setMessageBody:msgBody isHTML:NO];
    if (controller)
        [vc presentViewController:controller animated:YES completion:nil];
    
    _mailVC = vc;
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    
    [_mailVC dismissViewControllerAnimated:YES completion:nil];
}

@end
